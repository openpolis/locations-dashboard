/*
    Author Guglielmo Celata <guglielmo@openpolis.it>

    Copyright 2018 Fondazione Openpolis ETS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * OPMapThematizer singleton
 * an abstract, reusable object, that enables the user
 * to produce interactive d3-based maps (SVG), starting
 * from a topojson limits file and an indicator CSV file
 *
 * it reads:
 * - a topojson limits file, with at least 2 layers (com/prov or prov/reg),
 * - a csv file with indicator values for all areas in the layer
 *   and a shared id for the areas
 *
 * it produces a thematized choropleth map,
 * colored according to jenks classification,
 * which is draggable, zoomable, browseable (tooltips on mouse over areas)
 * and exportable as downloadable PNGs
 *
 * To use OPMapThematizer in an html page:
 *
 * - define html elements used by it in the DOM
 *     <div id="container" class="container-fluid outerdiv">
 *       <noscript>
 *           <div class="nojs">Javascript is either disabled or not supported in your browser. Please enable it or use a Javascript enabled browser.</div>
 *       </noscript>
 *       <h1></h1>
 *       <p id="loading-indicator" class="text-center lead">
 *           <i class="fa fa-spinner fa-spin fa-5x"></i><br/>
 *           Caricamento dei dati della mappa ...
 *       </p>
 *       <p class="notes"></p>
 *       <p class="source"></p>
 *       <svg id="map"></svg>
 *
 *       <!-- the positioning and zooming tools -->
 *       <div id="position-tools" class="btn-group animated bounce" role="group" style="display:none">
 *           <button  id="zoomIn" class="btn btn-default" title="Zoom In">
 *               <span class="glyphicon glyphicon-plus"></span>
 *           </button>
 *           <button  id="zoomOut" class="btn btn-default" title="Zoom Out">
 *               <span class="glyphicon glyphicon-minus"></span>
 *           </button>
 *           <button  id="zoomDef" class="btn btn-default">
 *               <span class="glyphicon glyphicon-resize-full" title="Zoom Reset"></span>
 *           </button>
 *           <button  id="limits" class="btn btn-default">
 *               <span class="glyphicon glyphicon-record" title="Mostra/Nascondi confini provinciali"></span>
 *           </button>
 *           <button  id="imgExport" class="btn btn-default">
 *               <span class="glyphicon glyphicon-camera" title="Esporta come immagine"></span>
 *           </button>
 *       </div>
 *
 *   </div>
 *
 * - load JS dependencies
 * <script src="https://unpkg.com/d3@4" charset="utf-8"></script>
 * <script src="https://unpkg.com/topojson-client@3"></script>
 * <script src="https://unpkg.com/d3-scale-chromatic@1"></script>
 * <script src="https://unpkg.com/d3-tip"></script>
 * <script src="/js/geostats.min.js"></script>
 * <script src="/js/canvas-toBlob.js"></script>
 * <script src="/js/FileSaver.min.js"></script>
 * <script src="/js/d3-opmap-thematizer.js"></script>
 *
 * - activate the thematizer after having defined
 *   all configuration options;
 * <script type="application/javascript">
 *       // all data and methods referencing
 *       // layers details MUST go here
 *       var MapThematizerConfig = {
 *       };
 *
 *       // activate the thematizer
 *       $(document).ready(function(){
 *           OPMapThematizer.init( MapThematizerConfig );
 *       });
 *   </script>
 */
OPMapThematizer = (function () {

    var _init = function( config ) {
        _mapWidth = config.width||600;
        _mapHeight = config.height||400;
        _limits_path = config.limits_path||"/static/data/limits.topo.json";
        _layer = config.layer;
        _indicatorReader = config.indicator_reader||{
            method: "csv",
            path: "data/indicators/demografia-dettagli-comunali-2015-reddito_medio-comuni.csv"
        };
        _indicatorMultiplier = parseFloat(
            config.indicatorMultiplier.replace(/,/g, '.')
        );
        _data = config.data;
        _getFeatures = config.getFeatures;
        _populateIndicatorById = config.populateIndicatorById;
        _colorize = config.colorize;
        _nColorClasses = config.nColorClasses||5;
        _minDomain = config.data.minDomain||0;
        _maxDomain = config.data.maxDomain||0;
        _tooltip = config.tooltip;
        _legendText = config.legendText||"AGGIUNGI TESTO LEGENDA";
        _superLimitsShown = config.superLimitsShownAtStart||1;
        _minZoomScale = config.minZoomScale||1;
        _maxZoomScale = config.maxZoomScale||12;

        q = d3.queue()
            .defer(d3.json, _limits_path);

        if (_indicatorReader.method === "csv") {
            q.defer(d3.csv, _indicatorReader.path);
        }

        q.await(_ready);
    };

    var _ready = function(error, limits, indicator) {
        d3.select("#loading-indicator").style("display", "none");
        if (error) throw error;

        if (_indicatorReader.method === "list"){
            if (_indicatorReader.list === undefined){
                throw "Need to specify indicator_reader.list parameter in config"
            }
            indicator = _indicatorReader.list;
        }
        var svg = d3.select("svg")
            .attr("width", _mapWidth)
            .attr("height", _mapHeight);

        d3.select("h1").text(_data.title);
        d3.select("p.notes").text(_data.notes);
        d3.select("p.source").text("Fonte: " + _data.source);
        d3.select("#position-tools").style("display", "block");

        // extract base and super layer from
        // topojson limits
        // uses _getFeatures() function, specified in config
        // that knows details about the limits layers
        var features = _getFeatures(limits, _layer);
        var super_layer = features[0],
            base_layer = features[1];

        // function to be applied to create path
        var projection = d3.geoMercator().fitSize(
            [_mapWidth, _mapHeight], base_layer
        );
        var path = d3.geoPath(projection);

        var indicatorById = _populateIndicatorById(
            _layer, indicator, _indicatorMultiplier
        );

        // uses https://github.com/simogeo/geostats to compute Jenks classes
        var indicatorValues = Object.keys(indicatorById).map(function (key) {
            return indicatorById[key];
        });
        var indicatorSerie = new geostats(indicatorValues);

        // define series domain, adjusting it
        // so that it starts from _minDomain and ends 10% over the max
        // thus showing the last color in the legend
        // maxDomain defaults to max value in series,
        // but can be overridden, setting the maxDomain config value
        var minDomain = _minDomain;
        indicatorSerie.getClassJenks(_nColorClasses);
        var colorDomain = indicatorSerie.bounds;
        colorDomain[0] = minDomain;
        if (_maxDomain !== 0 && colorDomain[_nColorClasses] > _maxDomain) {
            colorDomain[_nColorClasses] = _maxDomain;
        }
        maxDomain = _maxDomain + (_maxDomain - _minDomain) / 10.;
        // prepare scales and colors
        var x = d3.scaleLinear()
            .domain([minDomain, maxDomain])
            .range([_mapWidth - 240, _mapWidth - 40]);

        var colorRange = d3.schemeOrRd[colorDomain.length + 1];

        var color = d3.scaleThreshold()
            .domain(colorDomain)
            .range(colorRange);

        // assign color to datum
        function colorize(d) {
            return _colorize(d, _layer, color, indicatorById);
        }

        // setup tooltip
        // See original documentation for more details on styling:
        // http://labratrevenge.com/d3-tip/
        var tool_tip = d3.tip()
            .attr("class", "d3-tip")
            .offset([-8, 0])
            .html(function(d){
                return _tooltip(limits, d, _layer, indicatorById);
            });

        //
        // draw map
        //

        // add map container
        var mapg = svg.append("g").attr("id", "mapg");

        // base layer (multiple paths) eg: comuni
        mapg.append("g").attr("id", "base-layer")
            .selectAll("path")
                .data(base_layer.features)
                .enter().append("path")
                .attr("class", "area")
                .attr("d", path)
                .style("fill", colorize)
                .on("mouseover", function (d) {
                    d3.select(this).style("fill", "lightblue");
                    tool_tip.show(d);
                })
                .on("mouseout", function (d) {
                    d3.select(this).style("fill", colorize);
                    tool_tip.hide(d);
                });

        // super layer (single path) eg: province
        mapg.append("path")
            .attr("id", "super-layer")
            .datum(super_layer)
            .style("fill", "none")
            .style("stroke", "white")
            .style("stroke-width", 0.2)
            .style("stroke-linejoin", "round")
            .attr("d", path);


        // legend
        // add legend container
        var g = svg.append("g")
            .attr("class", "key")
            .attr("transform", "translate(0,40)");

        // add legend colors
        g.append("rect")
            .attr("y", -20)
            .attr("height", 45)
            .attr("x", x.range()[0] - 5)
            .attr("width", x.range()[1] - x.range()[0] + 10)
            .attr("fill", "#ffffff")
            .attr("fill-opacity", 0.4);
        g.selectAll("rect")
            .data(color.range().map(function(d) {
                d = color.invertExtent(d);
                if (d[0] === undefined) d[0] = x.domain()[0];
                if (d[1] === undefined) d[1] = x.domain()[1];
                return d;
            }))
            .enter().append("rect")
            .attr("height", 8)
            .attr("x", function(d) { return x(d[0]); })
            .attr("width", function(d) { return x(d[1]) - x(d[0]); })
            .attr("fill", function(d) { return color(d[0]); });

        // legend text
        g.append("text")
            .attr("class", "caption")
            .attr("x", x.range()[0])
            .attr("y", -6)
            .attr("fill", "#000")
            .attr("text-anchor", "start")
            .attr("font-weight", "bold")
            .text(_legendText);

        // legend values and ticks
        g.call(d3.axisBottom(x)
            .tickSize(13)
            .tickValues(color.domain())
            .tickFormat(function(d) {
                return parseInt(d +0.5);
            }));

        // remove legend border (make it nicer)
        g.select(".domain").remove();

        // add  tool tip behavior
        mapg.call(tool_tip);

        // add zoom interaction
        var minScale = _minZoomScale;
        var maxScale = _maxZoomScale;

        function _zoomed() {
            var transform = d3.event.transform;

            mapg.selectAll(".area").attr("transform", transform);
            mapg.select("#super-layer").attr("transform", transform);

            mapg.selectAll(".area").style("stroke-width", 0.2 / d3.event.transform.k + "px");
            mapg.select("#super-layer").style("stroke-width", 0.3 + "px");
        }

        var zoom = d3.zoom()
            .scaleExtent([minScale, maxScale])
            .on("zoom", _zoomed);

        var center = projection([12, 42]);

        mapg
            .call(zoom)
            .call(zoom.transform, d3.zoomIdentity
                .translate(_mapWidth / 2, _mapHeight / 2)
                .scale(minScale)
                .translate(-center[0], -center[1]));

        d3.select("#zoomIn")
            .on("click", function(){
                mapg.transition().duration(500).call(zoom.scaleBy, 1.618);
            });
        d3.select("#zoomOut")
            .on("click", function(){
                mapg.transition().duration(500).call(zoom.scaleBy, 0.618);
            });

        d3.select("#zoomDef")
            .on("click", function() {
                mapg.transition().duration(500)
                    .call(zoom.transform, d3.zoomIdentity
                        .translate(_mapWidth / 2, _mapHeight / 2)
                        .scale(minScale)
                        .translate(-center[0], -center[1])
                    );
            });

        // hide/show layers
        d3.select("#limits")
            .on("click", function() {
                prov = mapg.select("#super-layer")
                    .datum(super_layer);
                if (_superLimitsShown === 1){
                    _superLimitsShown = 0;
                    prov.style("stroke-width", 0 + "px");
                } else {
                    _superLimitsShown = 1;
                    prov.style("stroke-width", 0.3 + "px");
                }
            });
        d3.select(self.frameElement).style("height", _mapHeight + "px");

        // export button
        d3.select('#imgExport').on('click', function(){
            var svgString = _getSVGString(svg.node());
            _svgString2Image(
                svgString, 2*_mapWidth, 2*_mapHeight, 'png', save
            ); // passes Blob and filesize String to the callback

            function save( dataBlob, filesize ){
                saveAs( dataBlob, 'd3_export.png' ); // FileSaver.js function
            }
        });


    };

    // image exporting
    function _getSVGString( svgNode ) {
        svgNode.setAttribute('xlink', 'http://www.w3.org/1999/xlink');
        var cssStyleText = getCSSStyles( svgNode );
        appendCSS( cssStyleText, svgNode );

        var serializer = new XMLSerializer();
        var svgString = serializer.serializeToString(svgNode);
        svgString = svgString.replace(/(\w+)?:?xlink=/g, 'xmlns:xlink='); // Fix root xlink without namespace
        svgString = svgString.replace(/NS\d+:href/g, 'xlink:href'); // Safari NS namespace fix

        return svgString;

        function getCSSStyles( parentElement ) {
            var selectorTextArr = [];

            // Add Parent element Id and Classes to the list
            selectorTextArr.push( '#'+parentElement.id );
            for (var c = 0; c < parentElement.classList.length; c++)
                if ( !contains('.'+parentElement.classList[c], selectorTextArr) )
                    selectorTextArr.push( '.'+parentElement.classList[c] );

            // Add Children element Ids and Classes to the list
            var nodes = parentElement.getElementsByTagName("*");
            for (var i = 0; i < nodes.length; i++) {
                var id = nodes[i].id;
                if ( !contains('#'+id, selectorTextArr) )
                    selectorTextArr.push( '#'+id );

                var classes = nodes[i].classList;
                for (var c = 0; c < classes.length; c++)
                    if ( !contains('.'+classes[c], selectorTextArr) )
                        selectorTextArr.push( '.'+classes[c] );
            }

            // Extract CSS Rules
            var extractedCSSText = "";
            for (var i = 0; i < document.styleSheets.length; i++) {
                var s = document.styleSheets[i];

                try {
                    if(!s.cssRules) continue;
                } catch( e ) {
                    if(e.name !== 'SecurityError') throw e; // for Firefox
                    continue;
                }

                var cssRules = s.cssRules;
                for (var r = 0; r < cssRules.length; r++) {
                    if ( contains( cssRules[r].selectorText, selectorTextArr ) )
                        extractedCSSText += cssRules[r].cssText;
                }
            }


            return extractedCSSText;

            function contains(str,arr) {
                return arr.indexOf( str ) !== -1;
            }

        }

        function appendCSS( cssText, element ) {
            var styleElement = document.createElement("style");
            styleElement.setAttribute("type","text/css");
            styleElement.innerHTML = cssText;
            var refNode = element.hasChildNodes() ? element.children[0] : null;
            element.insertBefore( styleElement, refNode );
        }
    }

    function _svgString2Image( svgString, width, height, format, callback ) {
        var format = format ? format : 'png';

        var imgsrc = 'data:image/svg+xml;base64,'+ btoa( unescape( encodeURIComponent( svgString ) ) ); // Convert SVG string to data URL

        var canvas = document.createElement("canvas");
        var context = canvas.getContext("2d");

        canvas.width = width;
        canvas.height = height;

        var image = new Image();
        image.onload = function() {
            context.clearRect ( 0, 0, width, height );
            context.drawImage(image, 0, 0, width, height);

            canvas.toBlob( function(blob) {
                var filesize = Math.round( blob.length/1024 ) + ' KB';
                if ( callback ) callback( blob, filesize );
            });


        };

        image.src = imgsrc;
    }


    return {
        init: _init
    };

})();
